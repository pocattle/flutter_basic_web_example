class EpisodeItemModel {
  final String title;
  final double duration;
  final String imageUrl;

  EpisodeItemModel({
    this.title,
    this.duration,
    this.imageUrl,
  });

  EpisodeItemModel.fromJson(Map<String, dynamic> map)
      : title = map['title'],
        duration = map['duration'],
        imageUrl = 'https://64.media.tumblr.com/8a4d8a609a05d16bfa1cbf5226ae4784/tumblr_plte9mFTMo1udgjro_1280.jpg';
}
