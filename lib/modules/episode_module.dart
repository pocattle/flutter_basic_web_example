import 'package:flutter_modular/flutter_modular.dart';
import 'package:the_basics/routing/route_names.dart';
import 'package:the_basics/view_models/episode_view_model.dart';
import 'package:the_basics/views/episode_details/episode_details.dart';
import 'package:the_basics/views/episodes/episodes_view.dart';

class EpisodeModule extends Module {
  @override
  List<Bind> get binds => [
        Bind.lazySingleton((i) => EpisodeViewModel()),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute('/list', child: (_, args) => EpisodesView()),
        ChildRoute('/detail/:id',
            child: (_, args) =>
                EpisodeDetails(id: int.parse(args.params['id'])),
            children: [
              ChildRoute('', child: (_, __) => PageA()),
              ChildRoute('/ep2', child: (_, __) => PageB()),
            ]),
      ];
}
