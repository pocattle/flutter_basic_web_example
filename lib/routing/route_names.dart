const String HomeRoute = '/home';
const String AboutRoute = '/about';
const String EpisodesRoute = '/episodes';
const String EpisodeDetailRoute = '/detail';

const String EpisodeDetailFirstRoute = '/ep1';
const String EpisodeDetailSecondRoute = '/ep2';
