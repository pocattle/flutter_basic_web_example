import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:the_basics/datamodels/episode_item_model.dart';

class Api {
  final String _apiEndpoint =
      'https://us-central1-thebasics-2f123.cloudfunctions.net/thebasics';

  Future<dynamic> getEpisodes() async {
    final response = await http.get(Uri.parse('$_apiEndpoint/courseEpisodes'));

    if (response.statusCode == 200) {
      final episodes = (jsonDecode(response.body) as List)
          .map((e) => EpisodeItemModel.fromJson(e))
          .toList();
      return episodes;
    }

    return 'Could not fetch episodes';

  }

  Future<dynamic> getEpisode(int id) async {
    var response = await http.get(Uri.parse('$_apiEndpoint/episode?id=$id'));

    print(
        'getEpisode | response: ${response.body} statusCode: ${response.statusCode}');

    if (response.statusCode == 200) {
      var episode = EpisodeItemModel.fromJson(json.decode(response.body));
      return episode;
    }

    return 'Could not fetch episode $id. Make sure it exists';
  }
}
