import 'package:flutter/material.dart';
import 'package:the_basics/datamodels/navbar_item_model.dart';
import 'package:provider/provider.dart';

class NavBarItemTabletDesktop extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final model = context.watch<NavBarItemModel>();
    return Text(
      model.title,
      style: TextStyle(fontSize: 18),
    );
  }
}
