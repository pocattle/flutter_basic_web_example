import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:the_basics/datamodels/season_details_model.dart';
import 'package:the_basics/styles/text_styles.dart';
import 'package:provider/provider.dart';

class SeasonDetailsMobile extends StatelessWidget {
  const SeasonDetailsMobile({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final details = context.watch<SeasonDetailsModel>();
    return ResponsiveBuilder(
      builder: (context, sizingInformation) => Column(
        children: <Widget>[
          Text(
            details.title,
            style: titleTextStyle(sizingInformation.deviceScreenType),
          ),
          SizedBox(
            height: 50,
          ),
          Text(
            details.description,
            style: descriptionTextStyle(sizingInformation.deviceScreenType),
          ),
        ],
      ),
    );
  }
}
